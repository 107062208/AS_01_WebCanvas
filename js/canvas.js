var mousedown = false;
var currentMode = "";
var start_x = 0;
var start_y = 0;
var currentSize = 0;
var currentColor = "#000000";
var drawMode = 0;
var eraserMode = 0;
var drawPoly = 0;
var alreadyinput = 0;
var textmode = 0;

var undoArray = [];
var redoArray = [];

window.addEventListener("load", () => {
    //define canves
    const drawable = document.getElementById("canvas");
    const ctx = drawable.getContext("2d");
    ctx.lineCap = "round";

    document.getElementById("brush").addEventListener("click", () => {
        drawMode = 1;
        currentMode = "brush";
        document.getElementsByTagName("body")[0].style.cursor = "url(img/brush.png), auto";
    });

    document.getElementById("eraser").addEventListener("click", () => {
        currentMode = "eraser";
        eraserMode = 1;
        document.getElementsByTagName("body")[0].style.cursor = "url(img/eraser.png), auto";
    });

    document.getElementById("rect").addEventListener("click", () => {
        drawMode = 1;
        currentMode = "rect";
        document.getElementsByTagName("body")[0].style.cursor = "url(img/rect.png), auto";
    });

    document.getElementById("rect_block").addEventListener("click", () => {
        drawMode = 1;
        currentMode = "rectblock";
        document.getElementsByTagName("body")[0].style.cursor = "url(img/rect_block.png), auto";
    });

    document.getElementById("circle").addEventListener("click", () => {
        drawMode = 1;
        currentMode = "circle";
        document.getElementsByTagName("body")[0].style.cursor = "url(img/circle.png), auto";
    });

    document.getElementById("circle_block").addEventListener("click", () => {
        drawMode = 1;
        currentMode = "circleblock";
        document.getElementsByTagName("body")[0].style.cursor = "url(img/circle_block.png), auto";
    });

    document.getElementById("triangle").addEventListener("click", () => {
        drawMode = 1;
        currentMode = "triangle";
        document.getElementsByTagName("body")[0].style.cursor = "url(img/triangle.png), auto";
    });

    document.getElementById("triangle_block").addEventListener("click", () => {
        drawMode = 1;
        currentMode = "triangleblock";
        document.getElementsByTagName("body")[0].style.cursor = "url(img/triangle_block.png), auto";
    });

    document.getElementById("textbox").addEventListener("click", () => {
        drawMode = 1;
        textmode = 1;
        currentMode = "text";
        document.getElementsByTagName("body")[0].style.cursor = "url(img/font.png), text";
    });

    document.getElementById("reset").addEventListener("click", () => {
        undoArray.push(ctx.getImageData(0, 0, drawable.width, drawable.height));
        ctx.clearRect(0, 0, drawable.width, drawable.height);
    });

    document.getElementById("undo").addEventListener("click", () => {
        if (undoArray.length > 0) {
            redoArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
            ctx.putImageData(undoArray.pop(), 0, 0);
        }
    });

    document.getElementById("redo").addEventListener("click", () => {
        if (redoArray.length > 0) {
            undoArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
            ctx.putImageData(redoArray.pop(), 0, 0);
        }
    });

    document.getElementById("download").addEventListener("click", () => {
        var canvas = document.getElementById("canvas");
        image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        var link = document.createElement('a');
        link.download = "my_image.png";
        link.href = image;
        link.click();

    });

    drawable.addEventListener("mousedown", (e) => {
        mousedown = true;
        undoArray.push(ctx.getImageData(0, 0, drawable.width, drawable.height));
        redoArray = [];
        currentSize = document.getElementById("brush_size").value;
        ctx.lineWidth = currentSize;
        currentColor = document.getElementById("myColor").value;
        ctx.strokeStyle = currentColor;
        ctx.fillStyle = currentColor;
        start_x = e.offsetX;
        start_y = e.offsetY;
        if (currentMode == "brush" || currentMode == "eraser") {
            if (currentMode == "brush") ctx.globalCompositeOperation = "source-over";
            else ctx.globalCompositeOperation = "destination-out";
            ctx.beginPath();
            ctx.moveTo(e.offsetX, e.offsetY);
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.stroke();
        } else if (currentMode == "text") {
            ctx.globalCompositeOperation = "source-over";
            el = document.getElementById("font");
            ctx.font = document.getElementById("font_size").value + "px " + el.options[el.selectedIndex].value;
            ctx.fillText(document.getElementById("text").value, e.offsetX, e.offsetY);
        } else {
            ctx.globalCompositeOperation = "source-over";
        }
    });

    drawable.addEventListener("mousemove", (e) => {
        if (mousedown) {
            if (currentMode == "brush" || currentMode == "eraser") {
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.stroke();
                ctx.beginPath();
                ctx.moveTo(e.offsetX, e.offsetY);
            } else if (currentMode == "rect") {
                putPic();
                ctx.beginPath();
                ctx.rect(start_x, start_y, e.offsetX - start_x, e.offsetY - start_y);
                ctx.stroke();
            } else if (currentMode == "rectblock") {
                putPic();
                ctx.beginPath();
                ctx.fillRect(start_x, start_y, e.offsetX - start_x, e.offsetY - start_y);
                ctx.stroke();
            } else if (currentMode == "circle") {
                putPic();
                ctx.beginPath();
                ctx.arc(start_x, start_y, Math.sqrt(Math.pow(e.offsetX - start_x, 2) + Math.pow(e.offsetY - start_y, 2)), 0, 2 * Math.PI);
                ctx.stroke();
            } else if (currentMode == "circleblock") {
                putPic();
                ctx.beginPath();
                ctx.arc(start_x, start_y, Math.sqrt(Math.pow(e.offsetX - start_x, 2) + Math.pow(e.offsetY - start_y, 2)), 0, 2 * Math.PI);
                ctx.fill();
            } else if (currentMode == "triangle") {
                putPic();
                ctx.beginPath();
                ctx.moveTo(start_x, start_y);
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.lineTo(2 * start_x - e.offsetX, e.offsetY);
                ctx.closePath();
                ctx.stroke();
            } else if (currentMode == "triangleblock") {
                putPic();
                ctx.beginPath();
                ctx.moveTo(start_x, start_y);
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.lineTo(2 * start_x - e.offsetX, e.offsetY);
                ctx.closePath();
                ctx.fill();
            }
        }
    });

    window.addEventListener("mouseup", () => {
        mousedown = false;
        drawMode = 0;
        eraserMode = 0;
        drawPoly = 0;
    });
});

function putPic() {
    const drawable = document.getElementById("canvas");
    const ctx = drawable.getContext("2d");
    var tmp;
    ctx.putImageData(tmp = undoArray.pop(), 0, 0);
    undoArray.push(tmp);
}

function upload(e) {
    const drawable = document.getElementById("canvas");
    const ctx = drawable.getContext("2d");
    let file = new FileReader();
    file.readAsDataURL(e.target.files[0]);
    file.onload = (e) => {
        var img = new Image();
        img.src = e.target.result;
        img.onload = () => {
            ctx.drawImage(img, 10, 10, 640, 360);
        }
        undoArray.push(ctx.getImageData(0, 0, drawable.width, drawable.height));
    }
}