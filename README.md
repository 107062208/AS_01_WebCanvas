# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
```
關於js的部分，我先定義了一個canvas出來，接著，再用addEventListener，來記錄滑鼠的三種主要動作，分別是"mousedown","mousemove","mouseup"，再根據不同的mode，做出相對應的功能。接著解釋如何使用：
    
```

### 1.筆刷：
![](https://i.imgur.com/t7okMit.png)
```
點下筆刷的圖案，如需使用顏色，則點最左邊的顏色方塊，如需改變粗細，則點選色塊右邊的數字決定。
```
### 2.擦子：
![](https://i.imgur.com/IAr41Yp.png)
```
如筆刷一樣，點下即可開始使用，也可點選色塊右邊的數字決定粗細。
```
### 3.圖形系列：
![](https://i.imgur.com/ci0ajXj.png)
![](https://i.imgur.com/mM8vnuO.png)
![](https://i.imgur.com/p45xgV4.png)
![](https://i.imgur.com/7Stz8z2.png)
![](https://i.imgur.com/bvmfXv0.png)
![](https://i.imgur.com/PCIelsh.png)
```
都是點一下即可使用，另外也可選顏色。
```
### 4.文字：
![](https://i.imgur.com/2HEosuK.png)

```
在圖案"T"旁邊的框框打字，再右邊的框框可以選擇size跟字型，也可跟之前的方法一樣改變顏色，完成後點一下圖案"T"，接著游標出現後，在畫布上的任何位置放上text。
```
### 5.下載：
![](https://i.imgur.com/djF7WGI.png)

```
按下下載圖示即可以.png形式下載。
```
### 6.重置：
![](https://i.imgur.com/vAH3ZUh.png)

```
下載右邊的圖案是重置，可以清空畫布。
```
### 7.undo跟reod：
![](https://i.imgur.com/YQkiw3a.png)
![](https://i.imgur.com/c8JGvO4.png)
```
可以返回上一步跟取消返回上一步，由於我是用array去紀錄每一次的圖案，所以可以返回到任何步驟。
```

### 8.上傳：
```
點一下可以從電腦上傳照片。
```

### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

https://107062208.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
